class Fibonacci {
  static int calculate(int n) =>
      n <= 2 ? 1 : calculate(n - 2) + calculate(n - 1);
}
